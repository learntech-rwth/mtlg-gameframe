// see https://mtlg-framework.gitlab.io/api/MTLG.html for API
// see https://gitlab.com/mtlg-framework/mtlg-gameframe/wikis/mtlg/basic for the introduction in the wiki


// expose MTLG and createjs to the global namespace for debugging in the web developer console
// this is done in the framework in a later version
window.MTLG = MTLG;
window.window.createjs = createjs;

// start the framework as soon as the html page is loaded
document.addEventListener("DOMContentLoaded", MTLG.init);

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

// register the function that will define the playable levels
MTLG.addGameInit(initGame);

// setup gamelevels
function initGame() {
  MTLG.lc.registerLevel(mainLevel, () => true);
}





var globalVar = 'game.js';

function mainLevel() {
  var stage = MTLG.getStageContainer();

  var background = new createjs.Shape();
  background.graphics.beginFill("white").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);

  var textElement = new createjs.Text('', "50px Arial").set({x:20, y:100});

  var hint = new createjs.Text('run me once with `mtlg serve` and once with `mtlg serve module`', "30px Arial").set({x:20, y:500});

  stage.addChild(background, textElement, hint);


  var importedFunction = require('./other').otherFunction;
  var globalFunction = typeof otherFunction != 'undefined' ? otherFunction : (() => 'undefined');
  textElement.text =
`globalVar: ${globalVar}
importedFunction: ${importedFunction()}
globalFunction: ${globalFunction()}`
  ;
}
